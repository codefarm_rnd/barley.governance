/*
 *  Copyright (c) 2005-2013, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 *  WSO2 Inc. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */
package barley.governance.api.cache;

import javax.cache.event.CacheEntryCreatedListener;
import javax.cache.event.CacheEntryEvent;
import javax.cache.event.CacheEntryListenerException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import barley.core.context.PrivilegedBarleyContext;
import barley.governance.api.util.GovernanceUtils;
import barley.registry.core.Registry;
import barley.registry.core.exceptions.RegistryException;
import barley.registry.core.internal.RegistryCoreServiceComponent;

/**
 * This is a listener class to listen to cache entry creations
 *
 * @param <K>
 * @param <V>
 */
public class RXTConfigCacheEntryCreatedListener<K, V> implements CacheEntryCreatedListener<K, V> {
    private static final Log log = LogFactory.getLog(RXTConfigCacheEntryCreatedListener.class);

//	@Override
//	public void onCreated(Iterable<CacheEntryEvent<? extends K, ? extends V>> events)
//			throws CacheEntryListenerException {
//		// TODO Auto-generated method stub
//		
//	}

    @Override
    public void entryCreated(CacheEntryEvent<? extends K, ? extends V> cacheEntryEvent) throws CacheEntryListenerException {
        try {
            Registry registry = RegistryCoreServiceComponent.getRegistryService().getGovernanceSystemRegistry();
            GovernanceUtils.registerArtifactConfigurationByPath(registry, PrivilegedBarleyContext.getThreadLocalCarbonContext().getTenantId(), cacheEntryEvent.getKey().toString());
        } catch (RegistryException e) {
            log.error("Error while adding artifact configurations", e);
        }
    }
}
